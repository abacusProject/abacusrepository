package com.example.calculation;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ListView;
import android.widget.TextView;

public class MainActivity extends Activity
	{
		static Typeface tf;
		static List<String> menuNames;
		EfficientAdapter adap;

		// static List<Integer> imageIDs;
		@Override
		protected void onCreate(Bundle savedInstanceState)
			{
				super.onCreate(savedInstanceState);
				setContentView(R.layout.main_activity);
			}

		@Override
		protected void onResume()
			{
				// TODO Auto-generated method stub
				super.onResume();
				Initialize();

				ListView lv = (ListView) findViewById(R.id.listView1);
				adap = new EfficientAdapter(this);
				lv.setAdapter(adap);
				OnItemClickListener clickListener = new OnItemClickListener()
					{

						@Override
						public void onItemClick(AdapterView<?> arg0, View arg1,
								int arg2,
								long arg3)
							{
								GotoActivity(arg2);
							}
					};
				lv.setOnItemClickListener(clickListener);
			}

		void GotoActivity(int s)
			{
				if (s == 0)
					{

						Intent intent = new Intent(this, MehriyeActivity.class);
						startActivity(intent);
					}
				if (s == 1)
					{
						Intent intent = new Intent(this, Check.class);
						startActivity(intent);
					}
				if (s == 2)
					{
						Intent intent = new Intent(this, ArzeshAty.class);
						startActivity(intent);
					}
				if (s == 3)
					{
						Intent intent = new Intent(this, Vaam.class);
						startActivity(intent);
					}
				if (s == 4)
					{
						Intent intent = new Intent(this, Information.class);
						startActivity(intent);
					}
				if (s == 5)
					{
						Intent intent = new Intent(this, About.class);
						startActivity(intent);
					}

			}

		public void Initialize()
			{
				getContents();
				InitializeFonts();
				InitializeTextViews();
				InitializeButtons();
			}

		void InitializeTextViews()
			{

				// tf = Typeface.createFromAsset(getAssets(), "2.ttf");
				// TextView textView = (TextView)
				// findViewById(R.id.headerTextView);
				// textView.setTypeface(tf);
				// textView.setText(ResourceManager.getInstance().reshape("ØªÙ†Ø¸ÛŒÙ…Ø§Øª"));
				// tf = Typeface.createFromAsset(getAssets(), "10.ttf");
			}

		void getContents()
			{
				menuNames = new ArrayList<String>();
				menuNames.add("محاسبه مهریه");

				menuNames.add("محاسبه چک");

				menuNames.add("محاسبه ارزش آتی");
				menuNames.add("محاسبه اقساط وام");
				menuNames.add("راهنما ");
				menuNames.add("درباره ما ");

			}

		void InitializeFonts()
			{
				tf = Typeface.createFromAsset(getAssets(), "bnazanin.ttf");
			}

		void InitializeButtons()
			{
			}

		void GoBack()
			{
				finish();
			}

		private static class EfficientAdapter extends BaseAdapter implements
				Filterable
			{
				private LayoutInflater mInflater;

				public EfficientAdapter(Context context)
					{
						// Cache the LayoutInflate to avoid asking for a new one
						// each time.
						mInflater = LayoutInflater.from(context);
					}

				public View getView(final int position, View convertView,
						ViewGroup parent)
					{
						ViewHolder holder;
						if (convertView == null)
							{
								convertView = mInflater.inflate(
										R.layout.adapter_list, null);
								// Creates a ViewHolder and store references to
								// the two
								// children
								// views
								// we want to bind data to.
								holder = new ViewHolder();
								holder.menuName = (TextView) convertView
										.findViewById(R.id.menuNameTextView);
								holder.menuName.setTypeface(tf);
							}
						else
							{
								holder = (ViewHolder) convertView.getTag();
							}
						holder.menuName.setText(menuNames.get(position));
						return convertView;
					}

				static class ViewHolder
					{
						TextView menuName;
					}

				@Override
				public Filter getFilter()
					{
						return null;
					}

				@Override
				public long getItemId(int position)
					{
						return 0;
					}

				@Override
				public int getCount()
					{
						return menuNames.size();
					}

				@Override
				public Object getItem(int position)
					{
						return menuNames.get(position);
					}
			}

		@Override
		public boolean onCreateOptionsMenu(Menu menu)
			{
				// Inflate the menu; this adds items to the action bar if it is
				// present.
				getMenuInflater().inflate(R.menu.main, menu);
				return true;
			}

	}
