package com.example.calculation;

import helper.Calculator;
import android.app.Activity;
import android.app.Dialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MehriyeActivity extends Activity {

	OnClickListener clickListener = new OnClickListener() {

		@Override
		public void onClick(View arg0) {
			Calculator calculator = new Calculator();

			EditText valueEditText = (EditText) findViewById(R.id.aghdvalueEditText);
			String s = valueEditText.getText() + "";
			if (s == "") {

				Dialog d = new Dialog(MehriyeActivity.this);
				d.setContentView(R.layout.message);
				String title = getResources().getString(R.string.message_title);
				d.setTitle(title);
				d.show();
				return;
			}

			int val = Integer.parseInt(s);

			valueEditText = (EditText) findViewById(R.id.aghdyearEditText);
			s = valueEditText.getText() + "";
			if (s == "") {

				Dialog d = new Dialog(MehriyeActivity.this);
				d.setContentView(R.layout.message);
				String title = getResources().getString(R.string.message_title);
				d.setTitle(title);
				d.show();
				return;
			}
			int year = Integer.parseInt(s);

			float r = calculator.Mehriyeh(year, val);
			TextView text = (TextView) findViewById(R.id.resultEditText);
			text.setText(r + "");

			/*
			 * if ((EditText)findViewById(R.id.valueEditText)== null ||
			 * (EditText)findViewById(R.id.aghdyearEditText)== null){
			 * 
			 * Dialog d = new Dialog(MehriyeActivity.this);
			 * d.setContentView(R.layout.message); String title =
			 * getResources().getString(R.string.message_title);
			 * d.setTitle(title); d.show(); }
			 * 
			 * else {
			 */

			// InputMethodManager imm = (InputMethodManager)
			// getSystemService(INPUT_METHOD_SERVICE);
			// .hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);

		}

	};

	void Initialize() {
		InitializeButtons();
	}

	void InitializeButtons() {
		Button button = (Button) findViewById(R.id.calculateButton);

		button.setOnClickListener(clickListener);
	}

	void Calculate(int year, int val) {
		Calculator calculator = new Calculator();

		float r = calculator.Mehriyeh(year, val);
		TextView text = (TextView) findViewById(R.id.resultEditText);
		text.setText(r + "");

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.mehriyeh);
		Button button = (Button) findViewById(R.id.calculateButton);
		TextView text = (TextView) findViewById(R.id.aghdvalueTextView);
		TextView text1 = (TextView) findViewById(R.id.mehriyeTextView);
		TextView text2 = (TextView) findViewById(R.id.aghdyearTextView);
		TextView text3 = (TextView) findViewById(R.id.resultTextView);
		Typeface textfont = Typeface.createFromAsset(getAssets(), "bbc.ttf");
		button.setTypeface(textfont);
		text.setTypeface(textfont);
		text1.setTypeface(textfont);
		text2.setTypeface(textfont);
		text3.setTypeface(textfont);
		button.setOnClickListener(clickListener);

	}

}