package com.example.calculation;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class Information extends Activity {
	OnClickListener listener1 = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			finish();

		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.info);
		TextView text = (TextView) findViewById(R.id.checkTextView);

		text.setText("این نرم افزار شامل قسمت هایی از قبیل: "
				+ "\n\n 1.محاسبه مبلغ روز مهریه هایی که به صورت پول نقد تعیین شده اند . "
				+ "\n\n 2.محاسبه نرخ روز چکهای برگشتی"
				+ "\n\n 3.محاسبه ارزش آتی سرمایه"
				+ "\n\n4.محاسبه اقساط وام "
				+ "\n\n لازم به ذکر است محاسبه ی مهریه از سال 1315 ومحاسبه ی مبلغ چک بدهی و دیون ازسال 1369 قابل محاسبه میباشد.  ");
		Typeface textfont = Typeface.createFromAsset(getAssets(),
				"bbc.ttf");
		text.setTypeface(textfont);
		Button bt = (Button) findViewById(R.id.button3);
		bt.setTypeface(textfont);
		bt.setOnClickListener(listener1);
	}
}
