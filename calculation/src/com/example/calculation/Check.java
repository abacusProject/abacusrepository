package com.example.calculation;


import helper.CheckCalculator;
import android.app.Activity;
import android.app.Dialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Check extends Activity {
	OnClickListener clickListener1 = new OnClickListener() {

		@Override
		public void onClick(View arg0) {
			
			CheckCalculator calculator1 = new CheckCalculator();
			EditText valueEditText = (EditText) findViewById(R.id.checkvalueEditText);
			String s = valueEditText.getText() + "";
			if (s == "") {
				 Dialog p = new Dialog(Check.this);
             p.setContentView(R.layout.dontcare);
                 String title = getResources().getString(R.string.message_title);
                p.setTitle(title);
                p.show();
			 return;
			}
			long  valu = Integer.parseInt(s);
			
			EditText valueEditText1 = (EditText) findViewById(R.id.checkyearEditText);
			String r = valueEditText1.getText() + "";
			if (s == "") {
				 Dialog p = new Dialog(Check.this);
            p.setContentView(R.layout.dontcare);
                String title = getResources().getString(R.string.message_title);
               p.setTitle(title);
               p.show();
			 return;
			}
			int year = Integer.parseInt(r);
			EditText valueEditText2 = (EditText) findViewById(R.id.monthEditText);
			String n = valueEditText2.getText() + "";
			if (s == "") {
				 Dialog p = new Dialog(Check.this);
             p.setContentView(R.layout.dontcare);
                 String title = getResources().getString(R.string.message_title);
                p.setTitle(title);
                p.show();
			 return;
			}
			int month= Integer.parseInt(n);
			
		
		double t = calculator1.Check(valu,year, month);
			TextView text=(TextView)findViewById(R.id.checkresultTextView);
			text.setText(t+"");
		
		}
	};

	void Initialize() {
		InitializeButtons();
	}
	void InitializeButtons() {
		Button button = (Button) findViewById(R.id.computingButton);
		
		
		button.setOnClickListener(clickListener1);
	}
	
	void Computing(long valu,int year,int month) {
	CheckCalculator calculator1 = new CheckCalculator();
		
	
		
	
		
double t = calculator1.Check(valu,year, month);
		TextView text=(TextView)findViewById(R.id.checkresultTextView);
		text.setText(t+"");

	}

	   @Override
	    protected void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.check);
	    	Button button1 = (Button) findViewById(R.id.computingButton);
	    	TextView text=(TextView)findViewById(R.id.checkresultTextView);
	    	TextView text1=(TextView)findViewById(R.id.checkvalueTextView);
	    	TextView text2=(TextView)findViewById(R.id.checkyearTextView);
	    	TextView text3=(TextView)findViewById(R.id.checkTextView);
	    	TextView text4=(TextView)findViewById(R.id.monthTextView);
	    	
	    	Typeface textfont = Typeface.createFromAsset(getAssets(),
					"bbc.ttf");
	    	button1.setTypeface(textfont);
	    	text.setTypeface(textfont);
	    	text1.setTypeface(textfont);
	    	text2.setTypeface(textfont);
	    	text3.setTypeface(textfont);
	    	text4.setTypeface(textfont);
	    	
			button1.setOnClickListener(clickListener1);
	        
	    }


}
